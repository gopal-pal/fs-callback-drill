const fs = require("fs");
const path = require("path");

function operationsOfFS(callback) {
  try {
    if (typeof callback !== "function") {
      throw new Error("callback must be a function");
    } else {
      const filePath = path.join(__dirname, "Data");

      function read(path, cb) {
        return fs.readFile(path, "utf-8", (err, data) => {
          if (err) {
            cb(err, null);
          } else {
            cb(null, data);
          }
        });
      }
      function write(path, text, cb) {
        fs.writeFile(path, text, { encoding: "utf-8", flag: "a" }, (err) => {
          if (err) {
            cb(err, null);
          } else {
            cb();
          }
        });
      }

      const lipsumPath = path.join(filePath, "lipsum.txt");

      read(lipsumPath, (err, data) => {
        if (err) {
          throw err;
        } else {
          data = data.toString().toUpperCase();
          const uppercasePath = path.join(filePath, "uppercase.txt");

          write(uppercasePath, data, (err) => {
            if (err) {
              throw err;
            } else {
              console.log("uppercase file created");

              const filenamesPath = path.join(filePath, "filenames.txt");

              write(filenamesPath, "uppercase.txt", (err) => {
                if (err) {
                  throw err;
                } else {
                  console.log("new file name added");

                  read(uppercasePath, (err, data) => {
                    if (err) {
                      throw err;
                    } else {
                      data = JSON.stringify(data.toLowerCase().split("."));

                      const lowercasePath = path.join(
                        filePath,
                        "lowercase.txt"
                      );

                      write(lowercasePath, data, (err) => {
                        if (err) {
                          throw err;
                        } else {
                          console.log("lowercase.txt file added");
                        }
                      });
                      write(filenamesPath, "\nlowercase.txt", (err) => {
                        if (err) {
                          throw err;
                        } else {
                          console.log("new file added");

                          read(lowercasePath, (err, data) => {
                            if (err) {
                              throw err;
                            } else {
                              let dataArray = JSON.parse(data);
                              dataArray.sort();

                              const sortPath = path.join(filePath, "sort.txt");

                              write(
                                sortPath,
                                JSON.stringify(dataArray),
                                (err) => {
                                  if (err) {
                                    throw err;
                                  } else {
                                    console.log("sort.txt file created");

                                    write(
                                      filenamesPath,
                                      "\nsort.txt",
                                      (err) => {
                                        if (err) {
                                          throw err;
                                        } else {
                                          console.log("new file name added");

                                          read(filenamesPath, (err, data) => {
                                            if (err) {
                                              throw err;
                                            } else {
                                              let unlinkArray =
                                                data.split("\n");

                                              setTimeout(() => {
                                                for (let filename in unlinkArray) {
                                                  fs.unlink(
                                                    path.join(
                                                      filePath,
                                                      unlinkArray[filename]
                                                    ),
                                                    (err, data) => {
                                                      if (err) {
                                                        throw err;
                                                      }
                                                    }
                                                  );
                                                }
                                                console.log(
                                                  "All files deleted Successfully"
                                                );
                                              }, 3000);
                                            }
                                          });
                                        }
                                      }
                                    );
                                  }
                                }
                              );
                            }
                          });
                        }
                      });
                    }
                  });
                }
              });
            }
          });
        }
      });
    }
  } catch (err) {
    callback(err);
  }
}
module.exports = operationsOfFS;
