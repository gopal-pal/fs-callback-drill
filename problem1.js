const fs = require("fs");
const path = require("path");

function createAndRemovejsonFiles(directoryName, number, callback) {
  if (
    arguments.length < 3 ||
    typeof directoryName !== "string" ||
    typeof number !== "number"
  ) {
    throw new Error("Please follow the input format");
  }
  number = Math.floor(number);
  try {
    fs.mkdir(path.join(__dirname, directoryName), (err) => {
      if (err) {
        throw new Error("unable to create directory");
      }
    });
    callback(null, "New directory Created Successfully");

    for (let i = 1; i <= number; i++) {
      let filename = path.join(__dirname, directoryName, `${i}.json`);
      console.log(filename, " File Created");
      fs.writeFile(filename, "utf-8", (err) => {
        if (err) {
          throw new Error("unable to create the file");
        }
      });
    }

    callback(null, `${number} .JSON files are created`);

    setTimeout(() => {
      for (let i = 1; i <= number; i++) {
        let filename = path.join(__dirname, directoryName, `${i}.json`);

        fs.unlink(filename, (err) => {
          if (err) {
            throw new Error("unable to find file location");
          }
        });
        console.log(filename, " File Deleted");
      }
      callback(null, `${number} .Json files are deleted`);
    }, 4000);
  } catch (err) {
    callback(err);
  }
}

module.exports = createAndRemovejsonFiles;
